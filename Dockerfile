FROM node:13.5.0

# Update container
RUN apt-get update && \
    apt-get install -y git sudo

# enable sudo for uid=191 (which is apparently what Jenkins does)
RUN addgroup --gid 191 docker
RUN adduser --uid 191 --gid 191 --disabled-password --gecos '' docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Install UI testing requirements
#COPY requirements.txt /tmp/requirements.txt
#RUN pip3 install -r /tmp/requirements.txt

# Drop into bash
CMD ["/bin/bash"]
